package ru.t1.strelcov.tm.exception.empty;

import ru.t1.strelcov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error: Name is empty.");
    }

}
