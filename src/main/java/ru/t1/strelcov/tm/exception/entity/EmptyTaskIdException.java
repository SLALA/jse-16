package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public class EmptyTaskIdException extends AbstractException {

    public EmptyTaskIdException() {
        super("Error: Task id is empty.");
    }

}
