package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String value) {
        super("Error: Unknown command \"" + value + "\". Use \"help\" command.");
    }

}
