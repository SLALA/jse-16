package ru.t1.strelcov.tm.controller;

import ru.t1.strelcov.tm.api.controller.IProjectController;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static ru.t1.strelcov.tm.enumerated.Status.*;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("[Id]: " + project.getId());
        System.out.println("[Name]: " + project.getName());
        System.out.println("[Description]: " + project.getDescription());
        System.out.println("[Status]: " + project.getStatus().getDisplayName());
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + ":");
        List<Project> projects;
        final String sort = TerminalUtil.nextLine();
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            if (SortType.isValidByName(sort)) {
                final SortType sortType = SortType.valueOf(sort);
                final Comparator comparator = sortType.getComparator();
                projects = projectService.findAll(comparator);
                System.out.println(sortType.getDisplayName());
            } else
                throw new IncorrectSortOptionException(sort);
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
    }

    @Override
    public void findById() {
        System.out.println("[FIND PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void findByName() {
        System.out.println("[FIND PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void findByIndex() {
        System.out.println("[FIND PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void removeById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void updateByName() {
        System.out.println("[UPDATE PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        final String oldName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByName(oldName, name, description);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void startById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, IN_PROGRESS);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void startByName() {
        System.out.println("[START PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusByName(name, IN_PROGRESS);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void startByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeStatusByIndex(index, IN_PROGRESS);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void completeById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, COMPLETED);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void completeByName() {
        System.out.println("[COMPLETE PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusByName(name, COMPLETED);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void completeByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeStatusByIndex(index, COMPLETED);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
