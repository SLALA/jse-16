package ru.t1.strelcov.tm.controller;

import ru.t1.strelcov.tm.api.controller.IProjectTaskController;
import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("[Id]: " + task.getId());
        System.out.println("[Name]: " + task.getName());
        System.out.println("[Description]: " + task.getDescription());
        System.out.println("[Status]: " + task.getStatus().getDisplayName());
        System.out.println("[Project Id]: " + task.getProjectId());
    }

    @Override
    public void removeProjectWithTasks() {
        System.out.println("[REMOVE PROJECT WITH TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void findAllByProject() {
        System.out.println("[FIND ALL PROJECT'S TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTasksByProjectId(projectId);
        if (tasks != null) {
            int index = 1;
            for (final Task task : tasks) {
                System.out.println(index + ". " + task);
                index++;
            }
        }
    }

    @Override
    public void bindToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(taskId, projectId);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void unbindFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(taskId);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
