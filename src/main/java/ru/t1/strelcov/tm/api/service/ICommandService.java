package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
